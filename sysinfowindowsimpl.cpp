#include "sysinfowindowsimpl.h"

#include <windows.h>

SysInfoWindowsImpl::SysInfoWindowsImpl() :  SysInfo(), mCpuLoadLastValues() {

}

double SysInfoWindowsImpl::memoryUsed() {
    MEMORYSTATUSEX memoryStatus;
    memoryStatus.dwLength = sizeof (MEMORYSTATUSEX);
    GlobalMemoryStatusEx(&memoryStatus);
    quint64 memoryPhysicalUsed = memoryStatus.u11TotalPhys - memoryStatus.u11AvailPhys;
    return (double)memoryPhysicalUsed / (double)memoryStatus.u11TotalPhys * 100.0;
}

void SysInfoWindowsImpl::init() {
    mCpuLoadLastValues = cpuRawData();
}

QVector<quint64> SysInfoWindowsImpl::cpuRawData() {
    FILETIME idleTime;
    FILETIME kernelTime;
    FILETIME userTime;

    GetSystemTimes(&idleTime, &kernelTime, &userTime);

    QVector<quint64> rawData;
    rawData.append(convertFileTime(idleTime));
    rawData.append(convertFileTime(kernelTime));
    rawData.append(convertFileTime(userTime));
    return rawData;

}

quint64 SysInfoWindowsImpl::convertFileTime(const FILETIME &filetime) const {
    ULARGE_INTEGER largeInteger;
    largeInteger.LowPart = filetime.dwLowDateTime;
    largeInteger.HighPart = filetime.dwHighDateTime;
    return largeInteger.QuadPart;
}

double  SysInfoWindowsImpl::cpuLoadAverage() {
    QVector<quint64> firstSample = mCpuLoadLastValues;
    QVector<quint64> secondSample = cpuRawData();
    mCpuLoadLastValues = secondSample;

    quint64 currentIdle = secondSample[0] - firstSample[0];
    quint64 currentKernel = secondSample[1] - firstSample[1];
    quint64 currentUser = secondSample[2] - firstSample[2];
    quint64 currentSystem = currentKernel + currentUser;

    double percent = (currentSystem - currentIdle) * 100.0 / currentSystem;
    return qBound(0.0, percent, 100.0);
}
