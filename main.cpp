#include "mainwindow.h"
#include <QApplication>

#include "sysinfo.h"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    SysInfo::instance().init();
    MainWindow w;
    w.setWindowTitle("SysInfo");
    w.setGeometry(QRect(0,0,520,340));
    w.setMinimumWidth(260);
    w.setMinimumHeight(230);
    w.show();

    return a.exec();
}
