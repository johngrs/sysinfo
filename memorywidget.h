#ifndef MEMORYWIDGET_H
#define MEMORYWIDGET_H

#include <QObject>
#include <QWidget>

#include <QtCharts/QLineSeries>

#include "sysinfowidget.h"
#include "sysinfo.h"

class MemoryWidget : public SysInfoWidget {
    Q_OBJECT
  public:
    explicit MemoryWidget(QWidget *parent = nullptr);
  protected slots:
    void updateSeries() override;

  private:
    QLineSeries *mSeries;
    qint64 mPointPositionX;
};

#endif // MEMORYWIDGET_H
