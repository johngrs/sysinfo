#ifndef SYSINFOWINDOWSIMPL_H
#define SYSINFOWINDOWSIMPL_H

#include <QtGlobal>
#include <QVector>

#include "sysinfo.h"

typedef struct _FILETIME FILETIME;

class SysInfoWindowsImpl : public SysInfo {
  public:
    SysInfoWindowsImpl();

    void init() override;
    double cpuLoadAverage() override;
    double memoryUsed() override;

  private:
    QVector<quint64> cpuRawData();
    quint64 convertFileTime(const FILETIME &filetime) const;
    QVector<quint64> mCpuLoadLastValues;
};

#endif // SYSINFOWINDOWSIMPL_H
