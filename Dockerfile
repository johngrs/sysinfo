FROM blahord/gt5-dev
COPY . /usr/src/myapp
WORKDIR /usr/src/myapp
RUN qmake myapp.pro
RUN make
CMD ["./myapp"]

